﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchBlink : MonoBehaviour
{
    public float torchLoopTimer; //ogni quanto far partire la coroutine
    public float timerLowBattery; //ogni quanto lampeggia la torcia
    private bool lowBatteryIsActive = false;
    public Light torciaLight;



    public void Blink(float currentEnergy)
    {
        if(!lowBatteryIsActive)
        {
            if(currentEnergy <=  0.25)
            {
                StartCoroutine(BlinkRoutine(torchLoopTimer / 2, timerLowBattery));
            }
            else
            {
                StartCoroutine(BlinkRoutine(torchLoopTimer, timerLowBattery));
            }
           

        }
    }

    IEnumerator BlinkRoutine(float torchLoopTimer, float timerLowBattery)
    {
        lowBatteryIsActive = true;
        torciaLight.intensity = 0;
        yield return new WaitForSeconds(timerLowBattery);
        torciaLight.intensity = 5;
        yield return new WaitForSeconds(timerLowBattery);
        torciaLight.intensity = 0;
        yield return new WaitForSeconds(timerLowBattery);
        torciaLight.intensity = 5;
        yield return new WaitForSeconds(torchLoopTimer);
        lowBatteryIsActive = false;
        yield return null;
    }

    
}
