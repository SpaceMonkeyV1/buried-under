﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(Light))]
public class VolumetricLightMesh : MonoBehaviour
{
    public float maxOpacity = 0.25f;
    private MeshFilter filter;
    private Light light;

    private Mesh mesh;
    // Start is called before the first frame update
    void Start()
    {
        filter = GetComponent<MeshFilter>();
        light = GetComponent<Light>();

        if(light.type != LightType.Spot)
        {
            Debug.LogError("Hai usato un tipo di Light Mesh Volumentric che non è supportata. Usa le spotlight.");
        }
    }

    // Update is called once per frame
    void Update()
    {


        mesh = BuildMesh();

        filter.mesh = mesh;
    }

    private Mesh BuildMesh()
    {
        mesh = new Mesh();

        float farPosition = Mathf.Tan(light.spotAngle * 0.5f * Mathf.Deg2Rad) * light.range; //calcola la tangente del cono della luce per il suo raggio
        mesh.vertices = new Vector3[]
        {
            new Vector3(0, 0, 0),
            new Vector3(farPosition, farPosition, light.range),
            new Vector3(-farPosition, farPosition, light.range),
            new Vector3(-farPosition, -farPosition, light.range),
            new Vector3(farPosition, -farPosition, light.range),
        };

        mesh.colors = new Color[]
        {
            new Color(light.color.r, light.color.g, light.color.b, light.color.a * maxOpacity),
            new Color(light.color.r, light.color.g, light.color.b, 0),
            new Color(light.color.r, light.color.g, light.color.b, 0),
            new Color(light.color.r, light.color.g, light.color.b, 0),
            new Color(light.color.r, light.color.g, light.color.b, 0)
        };

        mesh.triangles = new int[]
        {
            0, 1, 2,
            0, 2, 3,
            0, 3, 4,
            0, 4, 1
        };

        return mesh;
    }
}
