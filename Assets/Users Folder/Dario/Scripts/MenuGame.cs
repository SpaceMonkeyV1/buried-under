﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuGame : Singleton<MenuGame>
{
    public GameObject MainMenu;
    public GameObject panelGame;
    public GameObject panelOption;
    public bool isPaused = false;

    private void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            pausePush();
        }
    }

    public void ResumeButton()
    {
        Time.timeScale = 1f;
        isPaused = false;
        panelGame.SetActive(false);
    }

    public void OptionsButton()
    {
        panelOption.SetActive(true);
    }

    public void ExitButton()
    {
        Time.timeScale = 1f;
        panelGame.SetActive(true);
        panelGame.transform.GetChild(0).gameObject.SetActive(false);
        panelGame.transform.GetChild(1).gameObject.SetActive(false);
        isPaused = false;
        Loader.LoadSceneAsync(Loader.BuildScene.MainMenu);
    }

    private void pausePush()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;

            if (isPaused)
            {
                SoundManager.instance.allSources.Player.Respiro.Pause();
                if (SoundManager.instance.allSources.Torcia.isPlaying)
                    SoundManager.instance.allSources.Torcia.Pause();
                panelGame.SetActive(true);
                Time.timeScale = 0f;
            }
            else
            {
                SoundManager.instance.allSources.Player.Respiro.UnPause();
                SoundManager.instance.allSources.Torcia.UnPause();
                Time.timeScale = 1f;
                panelGame.SetActive(false);
            }
        }
    }

    public IEnumerator InitMenu()
    {
        panelOption.GetComponentInChildren<BackButton>().toActive = panelOption;
        panelGame.SetActive(false);
        panelGame.transform.GetChild(0).gameObject.SetActive(true);
        panelGame.transform.GetChild(1).gameObject.SetActive(true);
        yield return null;
    }
}

