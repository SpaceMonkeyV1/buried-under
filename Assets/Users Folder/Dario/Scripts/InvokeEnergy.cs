﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class InvokeEnergy : MonoBehaviour
{
    [Header("Asset")]
    public Image energyImage;
    public GameObject ConoTorcia;
    [Header("Debug")]
    private PlayerMovement player;
    private MenuGame menuGame;
    public bool isCoroutineGoing = true;
    public bool DebugEnergy = true;
    public bool isON;
    public bool isFinished;
    [Header("Fear Variables")]
    public float timerEnergy = 0.1f;  //parametri per l'invoke
    public Light tociaLight;
    [Range(0, 100)]
    public float timerFear = 0;
    public float timerFearMultiplier = 1;
    public float timerFearDecreaser = 2;
    private TorchBlink torchBlink;
    [Header("Energy Variables")]
    private float currentEnergy;
    public float maxEnergy;
    public float energyDecreaser;

    [SerializeField, Tooltip("root inventario")]
    private GameObject inventario;
    public CameraScript cameraScript;

    public float CurrentEnergy
    {
        get => currentEnergy; set
        {
            currentEnergy = value;
            if (value < 0.5)
            {
                torchBlink.Blink(value);
            }
        }
    }

    private void Awake()
    {
        torchBlink = GetComponent<TorchBlink>();
        ConoTorcia.SetActive(false);
    }

    private void Start()
    {
        menuGame = FindObjectOfType<MenuGame>();
        player = FindObjectOfType<PlayerMovement>();
        energyImage = GetComponent<Image>();
        maxEnergy = energyImage.fillAmount;
        CurrentEnergy = maxEnergy;
        isCoroutineGoing = true;
        tociaLight.intensity = 0;
        DebugEnergy = true;
        timerFear = 0;
    }

    private void Update()
    {
        if (!isON)
        {
            SoundManager.instance.allSources.Torcia.Stop();
            ConoTorcia.SetActive(false);
        }
        else
        {
            if (!SoundManager.instance.allSources.Torcia.isPlaying)
                SoundManager.instance.allSources.Torcia.Play();
            if (!ConoTorcia.activeSelf)
                ConoTorcia.SetActive(true);
        }
            

        if (CurrentEnergy <= 0 || !isON)
        {   
            isON = false;
            if (!player.isOnCheck)
            {
                if(!inventario.activeSelf && !cameraScript.fullMapBoolean )
                {
                    timerFear += timerFearMultiplier * Time.deltaTime;
                }
            }
            energyImage.fillAmount = CurrentEnergy;
            tociaLight.intensity = 0;
        }
        else
        {
            if (CurrentEnergy >= 0)
            {
                CurrentEnergy -= energyDecreaser * Time.deltaTime;
            }
            energyImage.fillAmount = CurrentEnergy;
            timerFear -= timerFearMultiplier * Time.deltaTime;
        }

        AudioFear();
        TorchSwitch();
        StartCoroutine(FearCounter());
        ChangeColorBar();

        if(CurrentEnergy >= 1)
        {
            CurrentEnergy = 1;
        }
        timerFear = Mathf.Clamp(timerFear, 0, 101);
    }

    private void ChangeColorBar()
    {
        if (energyImage.fillAmount < 0.25f)
        {
            energyImage.color = Color.red;
        }
        else if (energyImage.fillAmount > 0.25 && energyImage.fillAmount < 0.5)
        {
            energyImage.color = Color.yellow;
        }
        else if (energyImage.fillAmount > 0.5 && energyImage.fillAmount < 0.75)
        {
            energyImage.color = Color.green;
        }
        else
        {
            energyImage.color = Color.white;
        }
    }
    

    private void TorchSwitch()
    {
        if (!inventario.activeSelf && !menuGame.isPaused) //se l'inventario è chiuso 
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && isON)
            {
                SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundTorch, "Light_off"));
                tociaLight.intensity = 0;
                isON = false;
            }
            else if (Input.GetKeyDown(KeyCode.Mouse0) && !isON && tociaLight.GetComponent<Light>().intensity <= 0 && CurrentEnergy > 0)
            {
                SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundTorch, "Light_on"));
                tociaLight.intensity = 5;
                isON = true;
            }
        }
    }

    IEnumerator FearCounter()
    {
        if (!inventario.activeSelf && !cameraScript.fullMapBoolean && !menuGame.isPaused) //se l'inventario è chiuso
        {
            if (!isON)
            {
                timerFear += Time.deltaTime * timerFearMultiplier;
                if (timerFear >= 100)
                {

                    player.DeathImage(true);
                }
            }
            else
            {
                timerFear -= Time.deltaTime * timerFearDecreaser;
                if (timerFear <= 0)
                {
                    timerFear = 0;
                }
            }
            yield return null;
        }
    }



    public void AudioFear()
    {
        bool isPlaying = SoundManager.instance.allSources.Player.Respiro.isPlaying;
        int fear = Mathf.FloorToInt(timerFear);

        Action<string> SetNewAudio = (stringa) => //Lambda 
        {
            AudioClip clip = SoundManager.GetAudioClip(SoundManager.instance.listSoundPlayer, stringa); //Trova l'audio
            SoundManager.instance.allSources.SetClipAndPlay(SoundManager.instance.allSources.Player.Respiro, clip); //Fallo partire
        };

        if (fear >= 0f && fear < 50f)
            if (!SoundManager.instance.allSources.Player.Respiro.clip.name.Equals("Respiro_lento"))
                SetNewAudio.Invoke("Respiro_lento");
        else if (fear >= 50 && fear < 75)
            if (!SoundManager.instance.allSources.Player.Respiro.clip.name.Equals("Respiro_medio"))
                SetNewAudio.Invoke("Respiro_medio");
        else if (fear > 75)
            if (!SoundManager.instance.allSources.Player.Respiro.clip.name.Equals("Respiro_forte"))
                SetNewAudio.Invoke("Respiro_forte");
    }
}
