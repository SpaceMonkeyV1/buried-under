﻿using UnityEngine;

public class Battery : MonoBehaviour
{
    public float recoveryEnergy;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                FindObjectOfType<InvokeEnergy>().CurrentEnergy += recoveryEnergy;
                Destroy(gameObject);
            }
        }
    }
}