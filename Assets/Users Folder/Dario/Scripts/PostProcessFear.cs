﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessFear : MonoBehaviour
{
    public InvokeEnergy energy;
    public PostProcessVolume volume;
    private Vignette _Vignette;
    private ChromaticAberration _ChromaticAberration;
    static float t = 0.0f;
    public float effectSpeed;
    public float multiplierEffect;
    public bool reverse = false;

    private void Start()
    {
        volume.profile.TryGetSettings(out _Vignette);
        volume.profile.TryGetSettings(out _ChromaticAberration);

        _Vignette.intensity.value = 0;
        _ChromaticAberration.intensity.value = 0;
    }

    private void Update()
    {
        if(energy.timerFear >= 50 && energy.timerFear <75)
        {
            FearEffects(effectSpeed);
        }
        else if (energy.timerFear >= 75 && energy.timerFear <= 100)
        {
            FearEffects(effectSpeed * multiplierEffect);
        }
        else
        {
            t = 0;
            _Vignette.intensity.value = 0;
            _ChromaticAberration.intensity.value = 0;
        }
        
    }

    private void FearEffects(float effectSpeed)
    {
        if (!reverse)
        {
            t += effectSpeed * Time.deltaTime;
            _ChromaticAberration.intensity.value = Mathf.Lerp(0, 1f, t);
            _Vignette.intensity.value = Mathf.Lerp(0, 1f, t);
            if (t > 1)
            {
                t = 1;
                reverse = true;
            }
                
        }
        else 
        {
            t -= effectSpeed * Time.deltaTime;
            _ChromaticAberration.intensity.value = Mathf.Lerp(0, 1f, t);
            _Vignette.intensity.value = Mathf.Lerp(0, 1f, t);
            if (t < 0)
            {
                t = 0;
                reverse = false;
            }
                
        }
    }

}