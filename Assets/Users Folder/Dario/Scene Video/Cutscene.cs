﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cutscene : MonoBehaviour
{
    [SerializeField]
    private float delayAfterChangeScene = 38f;

    private void Update()
    {
        delayAfterChangeScene -= Time.deltaTime;

        if(delayAfterChangeScene <= 0)
        {
            Loader.LoadSceneAsync(Loader.BuildScene.MainMenu);
        }
        
        if(Input.anyKeyDown)
        {
            delayAfterChangeScene = 0;
        }

    }
}
