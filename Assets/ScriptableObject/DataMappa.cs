﻿using UnityEngine;

[CreateAssetMenu(fileName = "Mappa-", menuName = "Data/Mappa", order = 0)]
public class DataMappa : ScriptableObject
{
    [Tooltip("La mappa è stata trovata?")]
    public bool isEnable;
    [Tooltip("Immagine sprite della mappa")]
    public Sprite imageMappa;
}
