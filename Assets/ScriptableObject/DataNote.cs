﻿using UnityEngine;

[CreateAssetMenu(fileName = "Note-", menuName = "Data/Notes", order = 1)]
public class DataNote : ScriptableObject
{
    [Tooltip("La nota è stata trovata durante il gioco?")]
    public bool isEnable;
    public string titolo;
    [TextArea(1, 20)]
    public string testo;
}