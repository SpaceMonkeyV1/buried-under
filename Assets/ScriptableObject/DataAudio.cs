﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

[CreateAssetMenu(fileName = "Audio-", menuName = "Data/Audio", order = 2)]
public class DataAudio : ScriptableObject
{
    [Tooltip("La nota audio è stata trovata durante il gioco?")]
    public bool isEnable;
    public string titolo;
    [TextArea(1, 20)]
    public string testo;

    [SerializeField, Tooltip("0.1 veloce - 0.5 lento"), Range(.1f, .5f)]
    private float awaitText = .1f;

    /**
     * <summary>Genera il testo stampando lettera per lettara, in un intervallo di secondi stabilito nello ScriptableObject</summary>
     * <param name="audioContainer"> GameObject che contiene l'audioContainer</param>
     * <param name="coroutine"> La coroutine che tiene presente se la funzione è stata portata a termine</param>
     */
    public IEnumerator GenerateDialogTextInAudio(GameObject audioContainer, Coroutine coroutine)
    { 
        var Testo = audioContainer.GetComponentInChildren<Image>().GetComponentInChildren<TMPro.TextMeshProUGUI>();

        Testo.alpha = 0f;
        Testo.text = testo;
        Testo.enableAutoSizing = true;
        Testo.ForceMeshUpdate();
        float idealFont = Testo.fontSize;
        Testo.enableAutoSizing = false;
        Testo.fontSize = idealFont;
        Testo.text = string.Empty;
        Testo.alpha = 1f;

        for (int i = 0; i < testo.Length; i++)
        {
            Testo.text += testo[i];
            if (!testo[i].Equals(' '))
                yield return new WaitForSeconds(awaitText);
        }

        yield return new WaitForEndOfFrame();
        DebugScript.ConsoleLog("Coroutine End : ", coroutine.ToString());
        coroutine = null;
    }
}