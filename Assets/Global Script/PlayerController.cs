﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Make a delegate Movement (NON URGENTE)

[RequireComponent(typeof(CharacterController)), Obsolete]
public class PlayerController : MonoBehaviour
{
	#region VARIABLES
    private Vector3 vectorMovement; //variabile di movimento
    private CharacterController characterController;
    private Manager gameManager; //variabili di managament
	
	private float jumpForce = 8f; //Forza di Salto	
    public InvokeEnergy energy;
    public Image energyImage;
    public float angoloY = 90f;
	
	private float playerGravity, playerVel, playerHeight;
    [SerializeField]
	[Tooltip("Il personaggio e' abbassato?")]
	private bool isCrouching = false;
    [SerializeField]
	[Tooltip("Il personaggio sta scalando la parete?")]
    private bool isClimbing = false;
    [SerializeField]
    [Tooltip("Il personaggio sta correndo? [1 = no, 2 = si]")]
    private bool isSprinting = false;


    [SerializeField]
    private GameObject inventario;
    private Camera MainCamera;
	#endregion
	
	#region PROPRETY
    public float AngoloY
    {
        get { return angoloY; }
        set
        {
            if (value <= 90 && value >= -90)
                angoloY = value;

        }
    }
	#endregion

	#region UNITY_METHODS
    private void Awake()
    {
        energyImage = FindObjectOfType<Image>();
        energy = FindObjectOfType<InvokeEnergy>();
        characterController = GetComponent<CharacterController>();
        gameManager = FindObjectOfType<Manager>();
        if (!gameManager)
            DebugScript.ConsoleLog("[ERROR]", "game manager not detected!", true);
        MainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        //inventario = GameObject.Find("Inventario").GetComponent<GameObject>();
    }

    private void Start()
    {
        vectorMovement = Vector3.zero;
        playerGravity = gameManager.PlayerGravity;
        playerVel = gameManager.PlayerVel;
        playerHeight = gameManager.PlayerHeight;
        jumpForce = gameManager.PlayerJumpForce;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Climbable"))// entra nel collider della scala
        {
            isClimbing = true;
        }
		else if (other.CompareTag("Mappa"))
        {
            Destroy(other.gameObject);
        }
        else if(other.gameObject.CompareTag("Spikes"))
        {
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            Death();
        }
        else if (other.CompareTag("Note"))
        {
            var note = new object();
            //StartCoroutine(inventario.GetComponentInParent<Inventory>().NewData(note as DataNote)); //Faccio un salto indietro
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Audio"))
        {
            var audio = other.gameObject.GetComponent<DataAudio>();
            //StartCoroutine(inventario.GetComponentInParent<Inventory>().NewData(audio)); //Faccio un salto indietro
            Destroy(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Climbable"))// esce dal collider della scala
        {
            isClimbing = false;
        }
    }
	
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("CheckPoint")) //TESTMINIMAP
        {
            collision.gameObject.layer = 0;
        }
    }

    private void Update()
    {
        TestVariable();


        //Sprint 
        if (Input.GetKey(KeyCode.LeftShift) && characterController.isGrounded)
        {
            isSprinting = true;
        }
        else isSprinting = false;

        Gravity();

        MakeJump();

        if (Input.GetKeyUp(KeyCode.R) && MainCamera.enabled)
        {
            inventario.SetActive(!inventario.activeSelf);
            if (inventario.activeSelf)
            {
                characterController.enabled = false; //i tasti per muovere il giocaore sono disa
            }
            else
            {
                characterController.enabled = true;
            }
        }
    }

    private void FixedUpdate()
    {
        MovementDirection();

        bool crouchButton = Input.GetKey(KeyCode.C);
        if (!isCrouching && crouchButton)//Crouch execution
        {
            isCrouching = true;
            characterController.height = playerHeight / 3; // altezza dimezzata
            characterController.center = new Vector3(characterController.center.x, characterController.center.y / 2, characterController.center.z);
            vectorMovement.x /= 2;//velocità dimezzata
        } 
        if (isCrouching && !crouchButton)
        {   //reset dei valori
            var cantStandUp = Physics.Raycast(transform.position, Vector3.up, playerHeight);
            //Debug.DrawRay(transform.position, Vector3.up* playerHeight, Color.green);
            if (!cantStandUp)
            {
                characterController.height = playerHeight;
                characterController.center = new Vector3(characterController.center.x, characterController.center.y * 2, characterController.center.z);
                isCrouching = false;
            }        
        }        

        if (characterController.enabled) //se la telecamera e' sulla mappa totale il giocatore in pou muoversi.
            characterController.Move(vectorMovement * Time.fixedDeltaTime); //funzione di movimento
        MakeJump();
    }

    #endregion

    #region MOVEMENT_METH
    private void MovementDirection()
    {
        AngoloY += Input.GetAxisRaw("Horizontal") * 3;//ROTAZIONE DEL PLAYER QUANDO CAMBIA DIREZIONE
        transform.rotation = Quaternion.Euler(0, AngoloY, 0);
        float sprintMult;
        if (isSprinting)
            sprintMult = 1.5f;
        else sprintMult = 1;

        //if (angoloY == 90 || angoloY == -90)
        //{
        //    vectorMovement = new Vector3(Input.GetAxisRaw("Horizontal") * playerVel * sprintMult, vectorMovement.y, 0); //CALCOLO MOVIMENTO PLAYER
        //}
        //else
        //{
        //    if(vectorMovement.x > 0)//BOH
        //        vectorMovement.x -= 1;
        //    if (vectorMovement.x < 0)
        //        vectorMovement.x += 1;
        //}

        vectorMovement = new Vector3(Input.GetAxisRaw("Horizontal") * playerVel * sprintMult, vectorMovement.y, 0); //CALCOLO MOVIMENTO PLAYER


        if (isClimbing)//CLIMBING EXECUTION
        {
            if (Input.GetAxisRaw("Vertical") != 0)
            {
                vectorMovement.y = Input.GetAxisRaw("Vertical") * playerVel / 2;
            }
        }
    }


    private void MakeJump() //ONLY TEST
    {
        if (characterController.isGrounded) //only test
        {
            if (Input.GetButtonDown("Jump"))//only test
            {
                vectorMovement.y = jumpForce; //only test
            }
        }
    }

    private void Gravity()
    {
        if (!characterController.isGrounded)
            vectorMovement.y += (Physics.gravity.y * playerGravity * Time.fixedDeltaTime);//Gestione della gravità
        else if (vectorMovement.y < -1)
            vectorMovement.y = -0.5f;//serve ad evitare che accumuli accellerazione verso il basso
    }

    private void TestVariable()
    {
        //ONLY FOR TESTING
        playerGravity = gameManager.PlayerGravity;
        playerVel = gameManager.PlayerVel;
        playerHeight = gameManager.PlayerHeight;
        jumpForce = gameManager.PlayerJumpForce;
        //ONLY FOR TESTING
    }
        
    public void Death()
    {
        characterController.enabled = false;
        characterController.transform.position = gameManager.lastCheckPoint;
        characterController.enabled = true;
        energy.CurrentEnergy = energy.maxEnergy;
        energy.timerFear = 0f;
    }
    #endregion
}
