﻿using UnityEngine;

public class CameraScript : MonoBehaviour
{
    #region VARIABLES
    private GameObject player;
    [Header("Camere di Gioco")]
    [SerializeField, Tooltip("Offset Camera giocatore")]
    private Vector3 o_Player;
    [SerializeField, Tooltip("Offset Camera Mappa")]
    private Vector3 o_Mappa;

    public bool fullMapBoolean = false;//boleana se la mappa è attiva o meno

    private GameObject root_imgage;

    private delegate void DelegateMapCamera(); //delegate mapFunc => A seconda del camera esegue la sua corretta funzione.
    DelegateMapCamera Camera;
    #endregion

    #region UNITY_METH
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (!player)
            DebugScript.ConsoleLog("[PLAYER]", "Player tag not found!", true);
        o_Player = new Vector3(0, 2, -10);

        root_imgage = FindObjectOfType<Inventory>().transform.GetChild(0).gameObject;
        if (root_imgage is null)
            DebugScript.ConsoleLog("[ERROR]", "InventarioRoot is null...", true);
        InitCamera();
    }

    private void Update()
    {
        Camera?.Invoke(); //Delegato
    }
    #endregion

    #region MY_METH
    private void InitCamera()
    {
        Camera = CompareTag("MainCamera") ? CameraMain : (DelegateMapCamera) FullMap; // Cast necesssario perché sennò il LAMBDA si incazza

        if (CompareTag("Mappa"))
            GetComponent<Camera>().enabled = false;
    }

    private void CameraMain()
    {
        transform.position = player.transform.position + o_Player;
        transform.LookAt(player.transform);
    }


    private void FullMap()
    {
        if (Input.GetKeyDown(KeyCode.M) && !root_imgage.activeSelf)
        {
            fullMapBoolean = !fullMapBoolean;
            SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundInventory, "Open_map"));
        }
            
        if (fullMapBoolean)
        {
            
            GetComponent<Camera>().enabled = true;
            player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().enabled = false;
            transform.position = o_Mappa;
        }
        else
        {
            GetComponent<Camera>().enabled = false;
            player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().enabled = true;
        }
    }
    #endregion
}
