﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ending : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //trigger main menu
            Loader.LoadSceneAsync(Loader.BuildScene.MainMenu);
        }
    }
}
