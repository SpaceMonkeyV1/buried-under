﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource), typeof(BoxCollider))]
public class SpecialAudio : MonoBehaviour
{
    private AudioSource source;

    [Header("Input Field")]
    [SerializeField, Tooltip("Riproduci solo una volta se true.")]
    private bool OneShot;
    [SerializeField, Tooltip("Lista con tutte le clip audio.")]
    private List<AudioClip> listAudioClips;

    [Header("Input")]
    [SerializeField, Tooltip("Timer di partenza in secondi.")]
    private float timerOnPlay;

    [Header("Input Random")]
    [SerializeField, Tooltip("Per abilitare il random")]
    private bool isRandom;
    [SerializeField, Tooltip("Min Range.")]
    private float minRange = 0f;
    [SerializeField, Tooltip("Max Range.")]
    private float maxRange = 0f;

    private int indexAudio;
    private bool startOnPlay;
    private float timer;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    private void Start()
    {
        timer = 0;
        startOnPlay = false;
        source.playOnAwake = false; //Non parte all'inizio
        source.loop = false; //Non va in loop
    }

    private void OnValidate()
    {            
        if (minRange > maxRange)
            minRange = maxRange - 1 < 0f ? 0f : maxRange - 1;

        if (isRandom)
            timerOnPlay = 0f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            if (!startOnPlay)   //Al momento questa fonte sonora è attiva?
                StartCoroutine(RandomSound());
    }

    private IEnumerator RandomSound()
    {
        //Init Variables
        startOnPlay = true;
        indexAudio = Random.Range(0, listAudioClips.Count);
        source.clip = listAudioClips[indexAudio];
        if (isRandom)
            timerOnPlay = Random.Range(minRange, maxRange);

        //Start timer
        while (timer < timerOnPlay)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //Play clip
        source.Play();
        while (source.isPlaying)
            yield return new WaitForEndOfFrame();

        //Re-Init Variables
        timer = 0;
        startOnPlay = false;
        yield return null;

        //Destroy Sentences
        if (OneShot)
            Destroy(gameObject);
    }
}