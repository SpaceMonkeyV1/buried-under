﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class MovementShip : MonoBehaviour
{
    #region Variabili
    public float actualVel = 1;
    public float friction = 0.1f;
    //public Vector3 oldPos;
    Rigidbody rb;

    public KeyCode up, down, left, right;
    #endregion

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        MovePhysics();
    }

    private void MovePhysics()
    {
        rb.velocity += GetDirection() * actualVel;
        rb.velocity -= Vector3.Scale(rb.velocity, new Vector3(friction, 0, friction));
        rb.angularVelocity *= friction;
    }

    public Vector3 GetDirection()
    {
        Vector3 dir = new Vector3(0, 0, 0);
        if (Input.GetKey(up))
        {
            dir += new Vector3(1, 0, 0);
        }
        if (Input.GetKey(down))
        {
            dir += new Vector3(-1, 0, 0);
        }
        if (Input.GetKey(left))
        {
            dir += new Vector3(0, 0, 1);
        }
        if (Input.GetKey(right))
        {
            dir += new Vector3(0, 0, -1);
        }
        dir.Normalize();
        return dir;
    }
}
