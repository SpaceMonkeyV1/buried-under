﻿using System;
﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    #region VARIABILI
    private Rigidbody rb;
    private Manager gameManager; //variabili di managament
    private Animator anim;
    private Coroutine coroutine;
    [Header("Ground e Collision")]
    public Transform groundTransform;
    public LayerMask groundMask;
    public BoxCollider upperBodyCollider;
    [Header("Energia")]
    public InvokeEnergy energy;
    public Image energyImage;
    public float multiplierCheckEnergy;
    public float multiplierCheckFear;
    [Header("Movimento")]
    public float jumpForce = 8f; //Forza di Salto
    public float rotationSpeed = 15f;
    public float speed = 100f;
    public float climbingSpeed = 100f;
    public float sprintMult = 1.5f;//moltiplicatore di velocità
    public float sprintTime = 3.0f;//Durata sprint consecutivo in secondi
    private float sprintTimer = 0f; //timer di durata sprint


    public float rotation;

    public Image imageDeath;
    public Sprite spriteDeathSpikes;
    public Sprite spriteDeathFear;
    private int climbingDirection;
    private float direction = 0f;
    private bool jump = false;
    private bool crouch = false;
    public bool isOnCheck = false;
    private bool isGrounded;
    private float sprint;
    private bool interactionAnimation = false;

    [SerializeField]
    [Tooltip("Il personaggio sta scalando la parete?")]
    private bool isClimbing = false;

    [SerializeField]
    [Tooltip("Il personaggio sta correndo? [1 = no, 2 = si]")]
    private bool isSprinting = false;

    public bool mouseRotation = false;

    [Header("Inventario")]
    [SerializeField, Tooltip("inserire root image.")]
    private GameObject inventarioRoot;
    [Header("Camera")]
    private Camera MainCamera;
    [Header("HUD")]
    private HUD_Script hud_Script;
    #endregion

    #region UNITY_METH
    private void Start()
    {
        
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        gameManager = FindObjectOfType<Manager>();
        if (!gameManager)
            print("[ERROR] game manager not detected!");

        rotation = rb.rotation.eulerAngles.y;

        energy = FindObjectOfType<InvokeEnergy>();
        energyImage = FindObjectOfType<Image>();
        MainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        hud_Script = FindObjectOfType<HUD_Script>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Mappa"))
        {
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("Spikes"))
        {
            DeathImage(false);
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            //Death();
        }
        if (other.CompareTag("Audio-Frammento"))
        {
            var Audio = other.GetComponent<DataFunction>().Audio;
            var Fragment = other.GetComponent<DataFunction>().Mappa;
            if (Audio != null)
            {
                interactionAnimation = true;
                SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundInventory, "Find_audio"));
                other.GetComponent<DataFunction>().inventoryScript.FindAndSetData(Audio);
                other.GetComponent<DataFunction>().Audio = null;
                hud_Script.RunNotification();
            }
            if (Fragment != null)
            {
                interactionAnimation = true;
                SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundInventory, "Find_map"));
                other.GetComponent<DataFunction>().inventoryScript.FindAndSetData(Fragment);
                other.GetComponent<DataFunction>().Mappa = null;
                hud_Script.RunNotification();
            }
        }
        if (other.CompareTag("CheckPoint"))
        {
            gameManager.lastCheckPoint = new Vector3(other.transform.position.x, other.transform.position.y, -0.6f);
        }
        if (other.CompareTag("Luce"))
        {
            var Note = other.GetComponent<DataFunction>().Note;
            if (Note != null)
            {
                interactionAnimation = true;
                SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundInventory, "Scribble_Sound"));
                other.GetComponent<DataFunction>().inventoryScript.FindAndSetData(Note);
                other.GetComponent<DataFunction>().Note = null;
                hud_Script.RunNotification();
            }
        }
    }
    private void DeFreezePlayer()
    {
        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        interactionAnimation = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Luce"))
        {
            isOnCheck = true;
            energy.timerFear -= energy.timerFearMultiplier * multiplierCheckFear * Time.deltaTime;
            energy.CurrentEnergy += energy.energyDecreaser * multiplierCheckEnergy * Time.deltaTime;
        }
        if (other.CompareTag("Climbable"))// esce dal collider della scala
        {
            isClimbing = true;
            climbingDirection = (int)other.GetComponent<ClimbableDirection>().climbingDirection;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Climbable"))// esce dal collider della scala
        {
            isClimbing = false;
        }
        if (other.CompareTag("Luce"))
        {
            isOnCheck = false;
        }
    }

    private void Update()
    {
        if (interactionAnimation)//Codice un sacco brutto.... ma funziona....
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
            print(rb.constraints);
            rotation = 0f;
            direction = 0;

            Invoke("DeFreezePlayer", 2);
        }
        else
            direction = Input.GetAxis("Horizontal");


        if (Input.GetButtonDown("Jump") && isGrounded && !interactionAnimation)
        {
            jump = true;
        }

        
        if (Input.GetKey(KeyCode.LeftShift) && sprintTimer < sprintTime && Mathf.Abs(rb.velocity.x) > .15f && !isClimbing)
        {
            sprintTimer += Time.deltaTime;
            isSprinting = true;
            sprint = sprintMult;
        }
        else
        {
            if (!Input.GetKey(KeyCode.LeftShift))
                sprintTimer -= Time.deltaTime;
            isSprinting = false;
            sprint = 1;
        }

        SetMovemtentSound(SoundManager.instance.allSources.Player);

        if (Input.GetButton("Crouch"))
        {
            crouch = true;
            anim.SetBool("Crouched", true);
        }
        else if (!Physics.Raycast(transform.position, Vector3.up, 1.4f, groundMask))
        {
            crouch = false;
            anim.SetBool("Crouched", false);
        }
        if (Physics.OverlapSphere(groundTransform.position, 0.25f, groundMask).Length > 0)
        {
            if (!isGrounded)
            {
                SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundPlayer, "Jump"));
                isGrounded = true;
            }
        }
        else
        {
            isGrounded = false;
        }
        
        if (Input.GetKeyUp(KeyCode.R) && MainCamera.enabled)
        {
            if (inventarioRoot.activeSelf && !inventarioRoot.GetComponentInParent<Inventory>().singleNote.activeSelf && !inventarioRoot.GetComponentInParent<Inventory>().singleAudio.activeSelf)
            {
                SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundInventory, "ZipApertura"));
                rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                inventarioRoot.SetActive(false);
            }
            else
            {
                SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundInventory, "ZipChiusura"));
                inventarioRoot.SetActive(true);
                rb.constraints = RigidbodyConstraints.FreezeAll;
            }
        }

        float feetDirection = (rotation / 90) * rb.velocity.normalized.x; //rotazione (tra -1 e +1) * la direction di movimento il tutto per capire la direzione della camminata
        anim.SetFloat("HorizontalDirection", feetDirection);
        anim.SetFloat("VerticalDirection", Mathf.Clamp(rb.velocity.y, -1, 1));
    }

    private void FixedUpdate()
    {
        var actualSpeed  = speed * sprint;
        rb.velocity = new Vector3(direction * actualSpeed, rb.velocity.y, 0);
        
        if (jump && isGrounded)
        {
            anim.Play("Jump");
            rb.velocity = Vector3.zero;
            rb.AddForce(new Vector3(0, jumpForce * 100, 0));
        }
        jump = false;

        if (crouch)
            upperBodyCollider.enabled = false;
        else upperBodyCollider.enabled = true;

        if (isClimbing)
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
                rotation = climbingDirection; // girati verso la Climbable
            rb.useGravity = false;
            if (Input.GetKey(KeyCode.W))
            {
                rb.velocity = new Vector3(rb.velocity.x, climbingSpeed, 0);//scalata in alto
            }
            else if (Input.GetKey(KeyCode.S))
            {
                rb.velocity = new Vector3(rb.velocity.x, -climbingSpeed, 0);//scalata in basso
            }
            else if (!jump)
                rb.velocity = new Vector3(rb.velocity.x, 0, 0); //Ti fermi dove sei sull'arrampicata
        }
        else
        {
            rb.useGravity = true;
        }
        

        //Spin
        if (!mouseRotation)
        {
            rotation += direction * rotationSpeed * Time.deltaTime;
            if (rotation > 90)
                rotation = 90;
            if (rotation < -90)
                rotation = -90;
            rb.rotation = Quaternion.Euler(0, rotation, 0);
        }
        else
        {
            rb.rotation = Quaternion.Euler(0, rotation, 0);
        }
        //ANIMATIONS
        anim.SetBool("Sprinting", false);
        anim.SetBool("Walking", false);

        if (rb.velocity.x != 0)
        {
            if (Mathf.Abs(rb.velocity.x) > speed +0.2)
            {
                anim.SetBool("Sprinting", true);
            }
            else anim.SetBool("Walking", true);
        }
        else
        {
            anim.SetBool("Sprinting", false);
            anim.SetBool("Walking", false);
        }

        if (isClimbing && !isGrounded)//Climbing animation
        {
            anim.SetBool("Climbing", true);
        }
        else anim.SetBool("Climbing", false);
    }
    #endregion

    private void SetMovemtentSound(SoundManager.AudioPlayer Player)
    {
        Action<string> SetNewAudio = (stringa) =>
        {
            var clip = SoundManager.GetAudioClip(SoundManager.instance.listSoundPlayer, stringa); //ricerca audio
            SoundManager.instance.allSources.SetClipAndPlay(Player.Movimento, clip); //set dell'audio
        };

        if (isSprinting) //sta correndo
        {
            if (!Player.Movimento.clip.name.Equals("Run"))
            {
                Player.Movimento.mute = false;
                SetNewAudio.Invoke("Run");
            }
                
            if (Mathf.Abs(rb.velocity.y) > .1f)
                Player.Movimento.Pause();
            else
                Player.Movimento.UnPause();
        }
        else if (isClimbing) //si sta arrampicando
        {
            if (!Player.Movimento.clip.name.Equals("Wall_climb"))
            {
                Player.Movimento.mute = false;
                SetNewAudio.Invoke("Wall_climb");
            }
            if (Mathf.Abs(rb.velocity.y) < .1f)
                Player.Movimento.Pause();
            else
                Player.Movimento.UnPause();
        }
        else
        {
            if (!Player.Movimento.clip.name.Equals("Walk"))
            {
                SetNewAudio.Invoke("Walk");
                Player.Movimento.mute = true;
            }

            if (Mathf.Abs(rb.velocity.x) < .1f || Mathf.Abs(rb.velocity.y) > .1f)
                Player.Movimento.Pause();
            else
                Player.Movimento.UnPause();
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(groundTransform.position, 0.25f);
        Gizmos.color = Color.blue;

        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x, transform.position.y + 1.4f, transform.position.z));
    }

    private void Death()
    {
        transform.position = gameManager.lastCheckPoint;
        energy.CurrentEnergy = energy.maxEnergy;
        energy.timerFear = 0f;
        hud_Script.Zaino.SetActive(true);
        imageDeath.enabled = false;
        coroutine = null;
    }

    private IEnumerator WaitAndLoad()
    {
        yield return new WaitForSecondsRealtime(2f);
        Death();
    }

    public void DeathImage(bool fear)
    {
        hud_Script.Zaino.SetActive(false); //disattiva l'hud Zaino
        if (coroutine == null)
        {
            imageDeath.enabled = true;
            if(fear)
            {
                imageDeath.sprite = spriteDeathFear;
            }
            else
            {
                imageDeath.sprite = spriteDeathSpikes;
            }

            coroutine = StartCoroutine(WaitAndLoad());
        }
    }
}
