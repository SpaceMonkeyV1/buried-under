﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    //La torcia segue il mouse sul piano Z
    #region VARIABLES
    public Camera mainCamera;
    private PlayerMovement player;
    private Plane zPlane = new Plane(new Vector3(0f, 0f, 1f), 0f);
    private Vector3 mousePos;
    [Range(0f, 90f)]
    public float upperAngleLimit = 60f;
    [Range(90f, 180f)]
    public float lowerAngleLimit = 160f;
    #endregion

    #region UNITY_METH
    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }
    
    private void Update()
    {
        Ray mousRay = mainCamera.ScreenPointToRay(Input.mousePosition);

        float enter;
        zPlane.Raycast(mousRay, out enter);
        mousePos = mousRay.origin + (mousRay.direction * enter);

        Vector3 pos = mousePos - transform.position; //posizione del mouse in relazione con la luce
        float angle = Mathf.Atan2(pos.x, pos.y) * Mathf.Rad2Deg;
        //DebugScript.ConsoleLog("Mouse relativePos = " + pos);

        //Restrizioni del movimento della luce
        if (!player.mouseRotation)
        {
            if (player.rotation > 0)
            {
                if (angle < 0)
                {
                    angle *= -1;
                }
                angle = Mathf.Clamp(angle, upperAngleLimit, lowerAngleLimit);
            }
            else
            {
                if (angle > 0)
                {
                    angle *= -1;
                }
                angle = Mathf.Clamp(angle, -lowerAngleLimit, -upperAngleLimit);
            }
        }
        else
        {
            float rotation = pos.x * 45;
            rotation = Mathf.Clamp(rotation, -90, 90);
            player.rotation = rotation;
            angle = Mathf.Clamp(angle, -lowerAngleLimit, lowerAngleLimit);
        }

        transform.rotation = Quaternion.Euler(angle - 90, 90, 0);
    }
    #endregion
}