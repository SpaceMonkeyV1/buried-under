﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private Manager gm;
    public GameObject respownPos;

    private void Start()
    {
        gm = FindObjectOfType<Manager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var pos = respownPos.transform.position;
            pos.z = 0;
            gm.lastCheckPoint = pos;
        }
    }
}
