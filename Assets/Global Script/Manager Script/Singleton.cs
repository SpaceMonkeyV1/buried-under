﻿using System;
using System.Collections;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T instance; //Richiama lo script statico

    protected virtual void Awake()
    {
        if (!instance)
        {
            if (typeof(T) != GetType())
            {
                Destroy(gameObject); //Distrugge le altre istanze degli script di tipo T
                throw new Exception("Singleton instance type mismatch!");
            }
            instance = this as T;
            StartCoroutine(DontDestroyTheParent(gameObject));
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator DontDestroyTheParent(GameObject child)
    {
        Transform parentTransform = child.transform;
        while (parentTransform.parent != null)
        {
            parentTransform = parentTransform.parent;
            yield return null;
        }

        DontDestroyOnLoad(parentTransform);
    }
}
