﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugScript : MonoBehaviour
{
    private InvokeEnergy DebugBool;
    private Light globalIllumination;
    private void Start()
    {
        DebugBool = FindObjectOfType<InvokeEnergy>();
        try //Utilizzo del try/catch per il debug di scene prototipo
        {
            globalIllumination = GameObject.FindGameObjectWithTag("GlobalLight").GetComponent<Light>();
            globalIllumination.enabled = false;
        }
        catch (System.NullReferenceException ex)
        {
            ConsoleLog("[LIGHT]", "GameObject with tag 'GlobalLigth' generate a error! ", true);
            ConsoleLog(ex.Message);
            globalIllumination = null;
        }
    }

    [System.Obsolete]
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.O)) //Reload current scene
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }
        else if (Input.GetKeyDown(KeyCode.P))//Stops Energy loss
        {
            DebugBool.DebugEnergy = !DebugBool.DebugEnergy;
        }
        else if (Input.GetKeyUp(KeyCode.Escape)) //Close application
        {
            Application.Quit();
        }
        else if (Input.GetKeyUp(KeyCode.L)) //enable global illumination
        {
            if (globalIllumination is null)
                ConsoleLog("[LIGHT]", "GameObject with tag 'GlobalLigth' not found! ", true);
            else
                globalIllumination.enabled = !globalIllumination.enabled; 
        }
    }

    public static void ConsoleLog(object item)
    {
        Debug.LogWarning(item.ToString());
    }

    public static void ConsoleLog(string _log, string _message, bool _isError = false)
    {
        if (_isError) Debug.LogError(_log + " : " + _message);
        else Debug.Log(_log + " : " + _message);
    }
}

