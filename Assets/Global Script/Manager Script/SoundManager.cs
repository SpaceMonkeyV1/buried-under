﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    [Serializable]
    public struct AudioPlayer
    {
        public AudioSource Respiro;
        public AudioSource Movimento;
    }
    
    [Serializable]
    public struct ManagerLoop
    {
        public AudioSource Background;
        public AudioPlayer Player;
        public AudioSource Torcia;

        public void MuteAllAudioSource(bool mute = true)
        {
            Background.mute = Player.Respiro.mute = Player.Movimento.mute = Torcia.mute = mute;
        }

        public void SetAllVolume(float volume)
        {
            Background.volume = Player.Respiro.volume = Player.Movimento.volume = Torcia.volume = volume;
        }

        public void SetClipAndPlay(AudioSource audio, AudioClip clip)
        {
            audio.Stop();
            audio.clip = clip;
            audio.Play();
        }
    }

    [Header("AudioSource"), Tooltip("Contenitore di tutti gli AudioSource")]
    public ManagerLoop allSources;
   
    [Header("Suoni Inventario"), Tooltip("Lista con tutti gli audio per L'inventario")]
    public List<AudioClip> listSoundInventory;
    [Header("Suoni Giocatore"), Tooltip("Lista con tutti gli audio per il Giocatore")]
    public List<AudioClip> listSoundPlayer;
    [Header("Suoni Torcia"), Tooltip("Lista con tutti gli audio per la Torcia")]
    public List<AudioClip> listSoundTorch;

    /**
     * <summary>Prendi un AudioClip da una lista audio del Sound Manager</summary>
     * <param name="list">La lista generica dove cercare l'audio.</param>
     * <param name="name">la stringa del nome dell'asset dell'audioclip.</param>
     */
    public static AudioClip GetAudioClip(List<AudioClip> list, string name)
    {
        for (int i = 0; i < list.Count; i++)
            if (list[i].name.Equals(name))
                return list[i];
        return null;
    }

    /**
     * <summary>Fa partire il suono di un'audioClip utilizza l'AudioSource "Background"</summary>
     * <param name="clip">L'AudioClip che viene eseguita.</param>
     */
    public void PlaySfx(AudioClip clip)
    {
        allSources.Background.PlayOneShot(clip);
    }

    public void SetAudioSource(GameObject[] audios, float volume)
    {
        for (int i = 0; i < audios.Length; i++)
            audios[i].GetComponent<AudioSource>().volume = volume;
    }
}