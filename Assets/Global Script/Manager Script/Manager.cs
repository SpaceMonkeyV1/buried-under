﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Manager : MonoBehaviour
{
    #region PLAYER_CONTROLLER_VAR
    [Header("Variabili Giocatore")]
    private PlayerMovement player;
    [Tooltip("Velocita del giocatore")]
    public float m_playerVel = 7f; //Velocità player (7 default)
    [Tooltip("Intensita della gravita")]
    public float m_playerGravity = 1f; //intensità gravità (1 default)
    [Tooltip("Barra delle'energia")]
    public InvokeEnergy energy;
    [Tooltip("Altezza del giocatore")]
    public float m_playerHeight = 1.8f; // altezza player (1.8 default)
    [Tooltip("Intensità di salto")]
    public float m_playerJumpForce = 8f; //intensità del salto (8f default)
    [Tooltip("Vettore con la posizione dell'ultimo checkpoint")]
    public Vector3 lastCheckPoint;
    #endregion

    #region PLAYER_CONTROLLER_PROPRETY
    public float PlayerVel
    {
        get { return m_playerVel; }
        set { m_playerVel = value; }
    }

    public float PlayerGravity
    {
        get { return m_playerGravity; }
        set { m_playerGravity = value; }
    }

    public float PlayerHeight
    {
        get { return m_playerHeight; }
        set { m_playerHeight = value; }
    }

    public float PlayerJumpForce
    {
        get { return m_playerJumpForce; }
        set { m_playerJumpForce = value; }
    }
    #endregion

    public PostProcessProfile pP;

    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
        lastCheckPoint = player.transform.position;
        pP = FindObjectOfType<PostProcessVolume>().profile;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                pP.GetSetting<AutoExposure>().keyValue.value += 0.2f;
            }
            else
            pP.GetSetting<AutoExposure>().keyValue.value -= 0.2f;

        }
    }

}
