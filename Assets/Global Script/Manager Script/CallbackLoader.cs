﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CallbackLoader : MonoBehaviour
{
    bool isFirtsFrame;

    private void Start()
    {
        isFirtsFrame = true;
    }

    private void Update()
    {
        if (isFirtsFrame)
        {
            isFirtsFrame = false;
            Loader.StartCallback();
        }
    }
}

public static class Loader
{
    public enum BuildScene : ushort
    {
        Loading,
        MainMenu,
        Level1,
    }
    
    [HideInInspector]
    public static Action onCallbackFunction;
    private static AsyncOperation asyncLoadOperator;
    private class LoadingMonoBehaviour : MonoBehaviour { }

    public static void LoadSceneAsync(int index)
    {
        onCallbackFunction = () =>
        {
            GameObject loadGameObject = new GameObject("Loader");
            loadGameObject.AddComponent<LoadingMonoBehaviour>().StartCoroutine(LoadNewSceneAsync(index));
        };

        SceneManager.LoadScene(BuildScene.Loading.ToString());
    }

    public static void LoadSceneAsync(BuildScene scene)
    {
        onCallbackFunction = () =>
        {
            GameObject loadGameObject = new GameObject("Loader");
            loadGameObject.AddComponent<LoadingMonoBehaviour>().StartCoroutine(LoadNewSceneAsync(scene));
        };

        SceneManager.LoadScene(BuildScene.Loading.ToString());
    }

    private static IEnumerator LoadNewSceneAsync(int index)
    {
        yield return null;
        asyncLoadOperator = SceneManager.LoadSceneAsync(index);
        while (!asyncLoadOperator.isDone)
            yield return new WaitForEndOfFrame();

        yield return new WaitForSeconds(2f);
    }

    private static IEnumerator LoadNewSceneAsync(BuildScene scene)
    {
        asyncLoadOperator = SceneManager.LoadSceneAsync(scene.ToString());
        while (!asyncLoadOperator.isDone)
        {
            Debug.Log(asyncLoadOperator.progress);
            yield return null;
        }

        yield return new WaitForSeconds(2f);
    }

    public static float GetLoaderProgressLerp(float MinBarValue, float MaxBarValue)
    {
        return asyncLoadOperator != null ? Mathf.Lerp(MinBarValue, MaxBarValue, Mathf.Clamp01(asyncLoadOperator.progress / .9f)) : MaxBarValue;
    }

    public static float GetLoaderProgressClamp(float MinBarValue, float MaxBarValue)
    {
        return asyncLoadOperator != null ? Mathf.Clamp(asyncLoadOperator.progress, MinBarValue, MaxBarValue) : MaxBarValue;
    }

    public static float GetLoaderProgressClamp01()
    {
        return asyncLoadOperator != null ? Mathf.Clamp01(asyncLoadOperator.progress / .9f) : 1f;
    }

    public static void StartCallback()
    {
        //!onLoaderCallback => i delegati non accettano operatore '!'
        if (onCallbackFunction != null)
        {
            onCallbackFunction.Invoke();
            onCallbackFunction = null;
        }
    }
}
