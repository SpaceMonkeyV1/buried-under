﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SliderOnLoading : MonoBehaviour
{
    private Slider Bar;

    private void Awake()
    {
        Bar = transform.GetComponent<Slider>();
    }

    private void Start()
    {
        Bar.value = Bar.minValue;
    }

    private void Update()
    {
        Bar.normalizedValue = Loader.GetLoaderProgressClamp(Bar.minValue, Bar.maxValue);
    }
}
