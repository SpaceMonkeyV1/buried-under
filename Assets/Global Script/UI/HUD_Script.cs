﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class HUD_Script : MonoBehaviour
{
    [Header("Zaino")]
    public GameObject Zaino;
    [SerializeField, Tooltip("Immagine del punto interrogativo in gioco.")]
    private Image questionMark;
    [Tooltip("Numero del Ciclio di sfumature (un ciclo sono tre lampeggi)"), Range(1, 5)]
    public int cycle = 1;
    [Tooltip("Quanto tempo ci deve mettere (in secondi) per fare un sigolo lampeggio"), Range(.1f, 4f)]
    public float timefadeOut = 1;

    [HideInInspector]
    public Coroutine coroutine;

    private void Awake()
    {
        if (!questionMark)
            DebugScript.ConsoleLog("[ERROR]", questionMark.name + " is null reference...", true);
    }

    public void RunNotification()
    {
        if (coroutine is null)
        {
            coroutine = StartCoroutine(AlphaEffect(questionMark, timefadeOut, cycle));
        }
    }

    private void LateUpdate()
    {
        if (Input.GetKeyUp(KeyCode.R) && questionMark.color.a == 1f)
            SetAlphaImage(questionMark, 0f);
    }

    private IEnumerator AlphaEffect(Image image, float timeFade = 1f, int cycle = 1)
    {
        int currentCycle = 0;
        do
        {
            yield return StartCoroutine(Cycle(image, timeFade));

        } while (++currentCycle != cycle && cycle > 0);

        yield return new WaitForEndOfFrame();
        coroutine = null;
    }

    private IEnumerator Cycle(Image image, float timeFade)
    {
        Color color = image.color;
        float time = 0f;
        while (time < timeFade)
        {
            time += Time.deltaTime;
            color.a = Mathf.Clamp01(time / timeFade);
            image.color = color;
            yield return null;
        }

        while (time > 0f)
        {
            time -= Time.deltaTime;
            color.a = Mathf.Clamp01(time / timeFade);
            image.color = color;
            yield return null;
        }

        while (time < timeFade)
        {
            time += Time.deltaTime;
            color.a = Mathf.Clamp01(time / timeFade);
            image.color = color;
            yield return null;
        }
    }

    private void SetAlphaImage(Image image, float val)
    {
        var color = image.color;
        color.a = val;
        image.color = color;
    }
}
