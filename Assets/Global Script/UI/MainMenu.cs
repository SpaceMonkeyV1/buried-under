﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [Header("Options")]
    [SerializeField, Tooltip("Pannello delle opzioni")]
    private GameObject optionPanel;
    [Header("Credits")]
    [SerializeField, Tooltip("Pannello dei Credits")]
    private GameObject creditsMenu;
    [SerializeField, Tooltip("Pannello dei Credits del dipartimento")]
    private GameObject creditsMenuDepartment;
    [SerializeField]
    private GameObject panelBadge;
    [SerializeField]
    private GameObject panelCredits;

    private Dictionary<GameObject, TextMeshProUGUI> allDepartment;
    private TextMeshProUGUI[] allTextMenu;

    private void Awake()
    {
        allDepartment = new Dictionary<GameObject, TextMeshProUGUI>();
        for (int i = 0; i < panelBadge.transform.childCount; i++)
        {
            GameObject key = panelBadge.transform.GetChild(i).gameObject;
            TextMeshProUGUI value = panelCredits.transform.GetChild(i).GetComponent<TextMeshProUGUI>();
            allDepartment.Add(key, value);
        }

        var MainPanel = transform.GetChild(0).gameObject;
        allTextMenu = new TextMeshProUGUI[MainPanel.transform.childCount];

        for (int i = 0; i < allTextMenu.Length; i++)
            allTextMenu[i] = MainPanel.transform.GetChild(i).GetComponentInChildren<TextMeshProUGUI>();
    }

    #region BUTTON_FUNC
    public void PlayGame()
    {
        StartCoroutine(FindObjectOfType<MenuGame>().InitMenu());
        Loader.LoadSceneAsync(Convert.ToInt32(Loader.BuildScene.Level1));
    }

    public void OpenOptionPanel()
    {
        TextReset();
        optionPanel.SetActive(true);
    }

    public void OpenCredits()
    {
        TextReset();
        creditsMenu.SetActive(true);
    }

    public void SingolCreditDepartment(GameObject Departement)
    {
        for (int i = 0; i < allDepartment.Count; i++)
        {
            var index = panelBadge.transform.GetChild(i).gameObject;
            if (allDepartment[index].enabled)
                allDepartment[index].enabled = false;
        }
        var header = creditsMenuDepartment.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
        header.text = Departement.GetComponentInChildren<TextMeshProUGUI>().text;
        allDepartment[Departement].enabled = true;
        creditsMenuDepartment.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    #endregion

    private void TextReset()
    {
        for (int i = 0; i < allTextMenu.Length; i++)
            allTextMenu[i].color = new Color(0f, 0f, 0f, 255f);
        transform.GetChild(0).gameObject.SetActive(false);
    }
}
