﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    #region VARIABLES
    private enum EnumInv : ushort { MAPPA, NOTE, AUDIO };

    public Image iMap, iNote, iAudio;

    [Header("Map UI Element")]
    [Tooltip("Prefab collegato al figlio Image del Panel della mappa.")]
    public GameObject panelMappaImageContent;
    private int indexSingleMap;
    public int IndexSingleMap
    { 
        get => indexSingleMap;
        set
        {
            int child = panelMappaImageContent.transform.childCount;
            indexSingleMap = (value < 0) ? child - 1 : (value >= child) ? 0 : value; //value è il numero successivo o precedente
        }
    }

    [Header("Note UI element")]
    [Tooltip("Prefab collegato al SingleNote.")]
    public GameObject singleNote;
    [Tooltip("Prefab collegato al Panel delle Note.")]
    public GameObject panelNoteContent;

    [Header("Audio UI element")]
    [Tooltip("Prefab collegato al SingleAudio.")]
    public GameObject singleAudio;
    [Tooltip("Prefab collegato al Panel degli Audio.")]
    public GameObject panelAudioContent;
    private int indexPageAudio;
    public int IndexPageAudio
    {
        get => indexPageAudio;
        set
        {
            int child = panelAudioContent.transform.childCount - 1;// L'ultima pagina/figlio non mi interessa
            indexPageAudio = (value < 0) ? child - 1 : (value >= child) ? 0 : value; //value è il numero successivo o precedente
        }
    }
    #endregion

    private void Awake()
    {
        if (!singleNote)
            DebugScript.ConsoleLog("[SINGOL NOTE]", "Singol Note is null reference", true);
        else if (!panelNoteContent)
            DebugScript.ConsoleLog("[CONTENT]", "panelNoteContent is null reference", true);
        else if (!singleAudio)
            DebugScript.ConsoleLog("[SINGOL AUDIO]", "Singol Audio is null reference", true);
        else if (!panelAudioContent)
            DebugScript.ConsoleLog("[CONTENT]", "panelAudioContent is null reference", true);

        StartCoroutine(InitInventario());
    }

    #region BUTTON_METH
    public void ChangeSchedeInventory(string name)
    {
        if (name.CompareTo("Mappa") == 0)
            SwitchMenuInventory(EnumInv.MAPPA);
        else if(name.CompareTo("Note") == 0)
            SwitchMenuInventory(EnumInv.NOTE);
        else
            SwitchMenuInventory(EnumInv.AUDIO);
    }

    public void NextMapImage()
    {
        var child = panelMappaImageContent.transform.GetChild(IndexSingleMap++);
        child.gameObject.GetComponent<Mask>().showMaskGraphic = false;
        child = panelMappaImageContent.transform.GetChild(IndexSingleMap);
        child.gameObject.GetComponent<Mask>().showMaskGraphic = true;
    }

    public void PrevMapImage()
    {
        var child = panelMappaImageContent.transform.GetChild(IndexSingleMap--);
        child.gameObject.GetComponent<Mask>().showMaskGraphic = false;
        child = panelMappaImageContent.transform.GetChild(IndexSingleMap);
        child.gameObject.GetComponent<Mask>().showMaskGraphic = true;
    }

    public void NextAudioPage()
    {
        var child = panelAudioContent.transform.GetChild(IndexPageAudio++);
        child.gameObject.SetActive(false);
        child = panelAudioContent.transform.GetChild(IndexPageAudio);
        child.gameObject.SetActive(true);
    }

    public void PrevAudioPage()
    {
        var child = panelAudioContent.transform.GetChild(IndexPageAudio--);
        child.gameObject.SetActive(false);
        child = panelAudioContent.transform.GetChild(IndexPageAudio);
        child.gameObject.SetActive(true);
    }
    #endregion

    #region MY_METH
    private void SwitchMenuInventory(EnumInv _switch)
    {
        switch (_switch)
        {
            case EnumInv.MAPPA:
                panelNoteContent.SetActive(false);
                panelAudioContent.SetActive(false);
                iNote.enabled = iAudio.enabled = false;
                panelMappaImageContent.transform.parent.gameObject.SetActive(true);
                iMap.enabled = true;
                break;
            case EnumInv.NOTE:
                panelMappaImageContent.transform.parent.gameObject.SetActive(false);
                panelAudioContent.SetActive(false);
                iMap.enabled = iAudio.enabled = false;
                panelNoteContent.SetActive(true);
                iNote.enabled = true;
                break;
            case EnumInv.AUDIO:
                panelMappaImageContent.transform.parent.gameObject.SetActive(false);
                panelNoteContent.SetActive(false);
                iMap.enabled = iNote.enabled = false;
                panelAudioContent.SetActive(true);
                iAudio.enabled = true;
                break;
        }
    }

    /**
    * <summary>Trova e Inserisce i dati nell'invenatrio avendo per parametro il dato di riferimento</summary>
    * <param name="mappa"> ScriptableObject riferito alla mappa</param>
    */
    public void FindAndSetData(DataMappa mappa)
    {
        DataFunction[] allMaps = panelMappaImageContent.GetComponentsInChildren<DataFunction>();
        for (int i = 0; i < allMaps.Length; i++)
        {
            if (allMaps[i].Mappa.Equals(mappa))
            {
                allMaps[i].Mappa.isEnable = true;
                var image = allMaps[i].gameObject.GetComponent<Image>();// panelMappaImageContent.transform.GetChild(i).gameObject.GetComponent<Image>();
                image.sprite = allMaps[i].Mappa.imageMappa;
                image.color = new Color(255f, 255f, 255f, .9f);
                break;
            }
        }
    }

    /**
    * <summary>Trova e Inserisce i dati nell'invenatrio avendo per parametro il dato di riferimento</summary>
    * <param name="note"> ScriptableObject riferito alla nota</param>
    */
    public void FindAndSetData(DataNote note)
    {
        DataFunction[] allNotes = panelNoteContent.GetComponentsInChildren<DataFunction>();
        for (int i = 0; i < allNotes.Length; i++)
        {
            if (allNotes[i].Note.Equals(note))
            {
                allNotes[i].Note.isEnable = true;
                allNotes[i].gameObject.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = allNotes[i].Note.titolo;
                break;
            }
        }
    }

    /**
     * <summary>Trova e Inserisce i dati nell'invenatrio avendo per parametro il dato di riferimento</summary>
     * <param name="audio"> ScriptableObject riferito all'audio</param>
     */
    public void FindAndSetData(DataAudio audio)
    {
        var Pagina = panelAudioContent.GetComponentsInChildren<RectTransform>();
        DataFunction[] allAudio;
        int index = 0;
        while (index < Pagina.Length)
        {
            allAudio = Pagina[index++].GetComponentsInChildren<DataFunction>();
            for (int i = 0; i < allAudio.Length; i++)
            {
                if (allAudio[i].Audio.Equals(audio))
                {
                    allAudio[i].Audio.isEnable = true;
                    allAudio[i].gameObject.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = allAudio[i].Audio.titolo;
                    index += Pagina.Length;
                    break;
                }
            }
        }

    }

    /**
     * <summary>Inizializza l'inventario portando a false tutti gli ScriptableObject dell'inventario</summary>
     */
    private IEnumerator InitInventario()
    {
        var Maps = panelMappaImageContent.GetComponentsInChildren<DataFunction>();

        for (int i = 0; i < Maps.Length; i++)
            Maps[i].Mappa.isEnable = false;

        var Notes = panelNoteContent.GetComponentsInChildren<DataFunction>();

        for (int i = 0; i < Notes.Length; i++)
            if(Notes[i].Note != null)
            Notes[i].Note.isEnable = false;

        var Pagine = panelAudioContent.GetComponentsInChildren<RectTransform>();
        DataFunction[] Audios;
        int index = 0;

        while (index < Pagine.Length)
        {
            Audios = Pagine[index++].GetComponentsInChildren<DataFunction>();
            if (Audios != null)
                for (int i = 0; i < Audios.Length; i++)
                    Audios[i].Audio.isEnable = false;
        }

        yield return new WaitForEndOfFrame();

        indexSingleMap = 0; //Segna la pagina iniziale
        for (int i = IndexSingleMap + 1; i < Maps.Length; i++)
            Maps[i].gameObject.GetComponent<Mask>().showMaskGraphic = false;

        for (int i = 0; i < Pagine.Length; i++)
        {
            if (Pagine[i].gameObject.activeSelf)
            {
                IndexPageAudio = i;
                break;
            }
        }

        yield return new WaitForEndOfFrame();

    }
    #endregion
}
