﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class BackButton : MonoBehaviour
{
    [Header("Back-Button-GameObject")]
    [SerializeField, Tooltip("GameObject da disabilitare")]
    public GameObject toDisable;
    [SerializeField, Tooltip("GameObject da abilitare")]
    public GameObject toActive;

    public void BackButtonFunction()
    {
        toActive.SetActive(true);
        toDisable.SetActive(false);
    }
}
