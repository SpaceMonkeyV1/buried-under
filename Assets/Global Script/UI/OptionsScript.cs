﻿using UnityEngine;
using UnityEngine.UI;

public sealed class OptionsScript : Singleton<OptionsScript>
{
    [SerializeField, Tooltip("Il panel che contiene la descrizione dei comandi.")]
    private GameObject panelCommands;
    private Canvas canvas;
    public bool isPaused;
    public Canvas canvasMenuGame;

    [SerializeField, Tooltip("Slider relativo al Volume")]
    private Slider Volume;
    //[SerializeField, Tooltip("Slider relativo al Gamma")]
    //private Slider Gamma;

    protected override void Awake()
    {
        isPaused = false;
        base.Awake();
        canvas = GetComponentInParent<Canvas>();
        if (!panelCommands)
            DebugScript.ConsoleLog("[ERROR]", panelCommands.name + "is null reference.", true);
    }

    private void LateUpdate()
    {
        if (canvas.worldCamera is null)
            canvas.worldCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    #region BUTTON_METH
    public void OpenPanelCommand()
    {
        panelCommands.SetActive(true);
    }

    public void ChangeVolumeInGame()
    {
        SoundManager.instance.allSources.SetAllVolume(Volume.value / Volume.maxValue);
        var special = GameObject.FindGameObjectsWithTag("SpecialAudio");
        if (special.Length != 0)
            SoundManager.instance.SetAudioSource(special, Volume.value / Volume.maxValue);
    }

    #endregion
}
