﻿using UnityEngine;
using UnityEngine.UI;

public class DataFunction : MonoBehaviour
{
    #region VARIABILI
    public DataNote Note;
    public DataAudio Audio;
    public DataMappa Mappa;

    [HideInInspector]
    public Inventory inventoryScript;
    
    private static Coroutine audioCoroutine;
    private static DataFunction audioInstance;
    #endregion

    private void Awake()
    {
        inventoryScript = FindObjectOfType<Inventory>(); //Darò per scontato che ci sarà solo un'inventario in scena.

        if (!inventoryScript)
            DebugScript.ConsoleLog("[ERROR]", "La variabile 'inventoryScript' e' null...", true);
    }

    #region BUTTON_FUNCTION
    public void OpenSingolNote()
    {
        if (inventoryScript.iNote.enabled && Note.isEnable) //In caso di variabile null non si generarno errori
        {
            inventoryScript.singleNote.SetActive(true);
            inventoryScript.singleNote.GetComponentInChildren<Text>().text = Note.testo;
        }
        else if (!Note)
            DebugScript.ConsoleLog("[ERROR]", "Note ScriptableObject is null.", true);
    }

    public void CloseSingolNote()
    {
        inventoryScript.singleNote.GetComponentInChildren<Text>().text = string.Empty;
        inventoryScript.singleNote.SetActive(false);
    }

    public void OpenSingolAudio() 
    {
        if (!Audio)
            DebugScript.ConsoleLog("[ERROR]", "Audio ScriptableObject is null.", true);
        else if (Audio.isEnable && audioCoroutine is null && audioInstance is null)
        {
            audioInstance = this; //Set dell'istanza
            inventoryScript.singleAudio.SetActive(true); //Apertura Single Audio
            SoundManager.instance.PlaySfx(SoundManager.GetAudioClip(SoundManager.instance.listSoundInventory, "Audio_switch_on"));
            audioCoroutine = audioInstance.StartCoroutine(audioInstance.Audio.GenerateDialogTextInAudio(audioInstance.inventoryScript.singleAudio, audioCoroutine)); //Attacco Coroutine tramite instance
        }
    }

    public void CloseAudioNote()
    {
        if (audioCoroutine != null)
        {
            audioInstance.StopCoroutine(audioCoroutine); //Chiusura Coroutine tramite instance
            audioCoroutine = null;
        }
        inventoryScript.singleAudio.GetComponentInChildren<Image>().GetComponentInChildren<TMPro.TextMeshProUGUI>().text = string.Empty;
        inventoryScript.singleAudio.SetActive(false);
        audioInstance = null; //Istanza set to null
    }
    #endregion
}
